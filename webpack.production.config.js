const path = require("path");

module.exports = {
    mode: "production",
    entry: {
        index: "./frontend/src/js/index.js",
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "frontend/dist/scripts"),
    },
    optimization: {
        minimize: true,
    },
    module: {
        rules: [
            {
                test: /\.(jsx|js)$/,
                include: path.resolve(__dirname, "frontend/src"),
                exclude: /node_modules/,
                use: [
                    {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                [
                                    "@babel/preset-env",
                                    {
                                        targets: "defaults",
                                    },
                                ],
                                "@babel/preset-react",
                            ],
                        },
                    },
                ],
            },
        ],
    },
};
