import React from "react";
import { MainContext } from "./main/maincontext.jsx";
import URLInput from "./main/urlinput.jsx";
import ShortURLDisplay from "./main/shorturldisplay.jsx";

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shortURL: "",
            setShortURL: (url) => {
                this.setState({ shortURL: url });
            },
        };
    }

    render() {
        return (
            <MainContext.Provider
                value={{
                    shortURL: this.state.shortURL,
                    setShortURL: this.state.setShortURL,
                }}
            >
                <section>
                    <h1>Marwynn's URL Shortener</h1>
                    <URLInput />
                    <ShortURLDisplay url={this.state.shortURL} />
                </section>
            </MainContext.Provider>
        );
    }
}
