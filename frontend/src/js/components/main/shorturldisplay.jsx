import React from "react";

export default class ShortURLDisplay extends React.Component {
    constructor(props) {
        super(props);
        this.state = { url: props.url };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.url !== this.props.url) {
            this.setState({ url: this.props.url });
        }
    }

    render() {
        return <a href={`https://${this.state.url}`}>{this.state.url}</a>;
    }
}
