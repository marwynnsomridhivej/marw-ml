import React from "react";
import { MainContext } from "./maincontext.jsx";

export default class URLInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = { url: "" };
        this.updateURL = this.updateURL.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    updateURL() {
        this.setState({ url: document.querySelector("input#url").value });
    }

    submitForm(setShortURL) {
        fetch("https://marw.ml/api/shorten", {
            method: "post",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                url: this.state.url,
            }),
        })
            .then((resp) => resp.json())
            .then((data) => {
                setShortURL(`marw.ml/${data.hash}`);
            });
    }

    render() {
        return (
            <MainContext.Consumer>
                {({ setShortURL }) => {
                    return (
                        <>
                            <div id="shorten-form">
                                <div id="form-content">
                                    <label for="url">Enter the URL</label>
                                    <input
                                        type="url"
                                        name="url"
                                        id="url"
                                        onChange={this.updateURL}
                                        required
                                    ></input>
                                </div>
                                <div id="submit-button">
                                    <button
                                        id="submit"
                                        onClick={() => {
                                            this.submitForm(setShortURL);
                                        }}
                                    >
                                        Get Shortened URL
                                    </button>
                                </div>
                            </div>
                        </>
                    );
                }}
            </MainContext.Consumer>
        );
    }
}
