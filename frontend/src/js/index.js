import React from "react";
import ReactDom from "react-dom";
import Main from "./components/index.jsx";

class App extends React.Component {
    render() {
        return (
            <>
                <Main />
            </>
        );
    }
}

ReactDom.render(<App />, document.getElementById("react-root"));
