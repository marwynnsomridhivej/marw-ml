const gulp = require("gulp");
const rename = require("gulp-rename");
const scss = require("gulp-sass");
const sources = ["index"];

sources.forEach((source) => {
    gulp.task(source, function () {
        return gulp
            .src([`./frontend/src/scss/${source}.scss`])
            .pipe(scss({ outputStyle: "compressed" }))
            .pipe(rename(`${source}.css`))
            .pipe(gulp.dest("./frontend/dist/styles"));
    });
});

gulp.task("default", gulp.series(sources));
