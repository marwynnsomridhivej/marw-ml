import os

from dotenv import load_dotenv
from sanic import Blueprint
from sanic.request import Request
from sanic.response import json

from .utils import CORS, create_shortened_url

load_dotenv(os.path.abspath("./.env"))

api = Blueprint("API", host=os.getenv("MAIN_HOST"))


@api.route("/api/shorten", methods=["POST"])
async def api_shorten(request: Request) -> json:
    data = request.json
    # {
    #     "url": "https://youtube.com/watch?v=3kjshd_s8",
    #     "customID": "",
    #     "userID": "",
    # }
    if not data.get("url", None):
        payload = {
            "message": "Invalid request data"
        }
        status = 400
    else:
        _hash = await create_shortened_url(request.app.db)
        async with request.app.db.acquire() as con:
            await con.execute(
                f"INSERT INTO urls(original_url, hash) VALUES ($text${data['url']}$text$, $text${_hash}$text$)"
            )
        payload = {
            "hash": _hash,
        }
        status = 200
    return json(payload, status=status, headers=CORS)
