import os

from aiofile import async_open
from dotenv import load_dotenv
from sanic import Blueprint
from sanic.request import Request
from sanic.response import html, redirect

load_dotenv(os.path.abspath("./.env"))

main = Blueprint("Main", host=os.getenv("MAIN_HOST"))


@main.route("/", methods=["GET"])
async def landing(request: Request) -> html:
    async with async_open(os.path.abspath("./frontend/index.html"), "r") as index:
        page = await index.read()
    return html(page, status=200)


@main.route("/<_hash>", methods=["GET"])
async def hashroute(request: Request, _hash: str) -> redirect:
    async with request.app.db.acquire() as con:
        original_url = await con.fetchval(
            f"SELECT original_url FROM urls WHERE hash=$text${_hash}$text$"
        )
    return redirect(original_url, status=301)
