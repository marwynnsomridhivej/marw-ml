import random
import string

_VALID_CHARS = string.ascii_letters + string.digits


async def create_shortened_url(db) -> str:
    async with db.acquire() as con:
        while True:
            _hash = "".join(random.choices(_VALID_CHARS, k=6))
            exists = await con.fetchval(
                f"SELECT original_url FROM urls WHERE hash=$text${_hash}$text$ LIMIT 1"
            )
            if not exists:
                break
    return _hash
