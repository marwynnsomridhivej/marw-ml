from .cors import _CORS as CORS
from .shortenerhash import create_shortened_url
