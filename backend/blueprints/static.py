import os

from sanic import Blueprint

_RESOURCES = [
    "scripts",
    "styles",
    "static",
]
static = Blueprint("Static")

for resource in _RESOURCES:
    static.static(f"/{resource}", os.path.abspath(f"./frontend/dist/{resource}"))
