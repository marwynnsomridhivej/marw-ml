import asyncio
import os

import asyncpg
from dotenv import load_dotenv
from sanic import Sanic
from sanic.exceptions import NotFound
from sanic.request import Request
from sanic.response import json

from blueprints import api, main, static

try:
    import uvloop
except ImportError:
    pass
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
finally:
    loop = asyncio.get_event_loop()


load_dotenv(os.path.abspath("./.env"))

app = Sanic(__name__)
app.blueprint([
    api,
    main,
    static,
])


@app.listener("before_server_start")
async def setup_db(app: Sanic, loop: asyncio.AbstractEventLoop) -> None:
    db = await asyncpg.create_pool(**{
        "user": os.getenv("PG_USER"),
        "password": os.getenv("PG_PASSWORD"),
        "database": os.getenv("PG_DATABASE"),
        "host": os.getenv("PG_HOST")
    })
    async with db.acquire() as con:
        await con.execute(
            "CREATE TABLE IF NOT EXISTS urls(hash TEXT PRIMARY KEY, original_url TEXT,"
            "customID TEXT DEFAULT NULL, userID TEXT DEFAULT NULL, og_title TEXT DEFAULT NULL, "
            "og_description TEXT DEFAULT NULL, og_image TEXT DEFAULT NULL)"
        )
    app.db = db


@app.exception(NotFound)
async def handle_not_found(request: Request, exception: NotFound) -> json:
    return json({
        "message": "The requested resource was not found",
    }, status=exception.status_code)


if __name__ == "__main__":
    srv_coro = app.create_server(host=os.getenv("HOST"), port=os.getenv("PORT"), return_asyncio_server=True)
    srv = loop.run_until_complete(srv_coro)
    try:
        loop.run_until_complete(srv.start_serving())
        loop.run_until_complete(srv.serve_forever())
    except KeyboardInterrupt:
        srv.close()
        loop.close()
