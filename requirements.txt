aiofile==3.3.3
aiohttp==3.7.3
asyncpg==0.21.0
python-dotenv==0.15.0
sanic==20.12.1
uvloop==0.14.0